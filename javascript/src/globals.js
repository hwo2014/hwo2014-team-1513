var globals = {};


globals.getPiece = function(index) {
  index = index % globals.race.track.pieces.length;
  return globals.race.track.pieces[index];
};

globals.isNextNPieceStraight = function(n, index) {
  var yes = 1,
    i = 0,
    ind;
    
  for(;i < n; i += 1) {
    ind = (index + i) % globals.race.track.pieces.length;
    if(globals.getPiece(ind).angle) {
      yes = 0;
      break;
    }
  }
  return yes;
};

globals.insertDeccel = function(speed, accel) {
  accel = Math.abs(accel);
  
  if(speed <= 0 || speed > 10 || accel > 1) {
    return;
  }
  
  var i = globals.deccelFreq.indexOf(speed);
  
  if(i === -1) {
    
    i = 0;   
    
    while(i < globals.deccelFreq.length && globals.deccelFreq[i] > speed) {
        i += 1;
    }

    if(i == -1) {
      globals.deccelFreq.unshift(speed);
      globals.deccel.unshift(accel);
    } else if(i === globals.deccelFreq.length) {
      globals.deccelFreq.push(speed);
      globals.deccel.push(accel);
    } else {
      globals.deccelFreq.splice(i, 0, speed);
      globals.deccel.splice(i, 0, accel);
    }

  } else {
    globals.deccel[i] = accel;
  }
};

/*globals.getMaxStraight = function() {
  var ret = {
    "start": 0,
    "end": 0,
    "length": 0
    },
    i = 0,
    len,
    maxLen = 0,
    count = 0,
    piece;
  
  for(; i < globals.race.track.pieces.length; i += 1) {
    len = 0;
    count = 0;
    
    while(globals.isNextNPieceStraight(len, i)) {
      len += 1;
      piece = globals.getPiece((i + len - 1) % globals.race.track.pieces.length);
      count += piece.length || 0;
    }
    
    if(len > maxLen) {
      ret.start = i;
      ret.end = (i + len - 1) % globals.race.track.pieces.length;
      ret.length = count;
    }
  }
  console.log(globals.getPiece(4));
  return ret;
};*/

globals.cloneTurns = function() {
  var newTurns = {}, arr = Object.keys(globals.turns), ret;
  for(var i = 0; i < arr.length; i += 1) {
    ret = {};
    ret.s = globals.turns[arr[i]].s;
    if(globals.turns[arr[i]].final) {
      ret.final = 1;
    }
    newTurns[arr[i]] = ret;
  }
  return newTurns;
};

globals.deccelrateTo = function(speed, to, length) {
  var i = 0,
    dec,
    spd;
  
  if(!speed || speed < 0 || !to || to < 0) {
    return 0;
  }
  
  if(Math.abs(speed - to) < 0.1) {
    return length;
  }  
  
  for(; i < globals.deccelFreq.length; i += 1) {
    if(globals.deccelFreq[i] < speed) {
      break;
    }
  }
  
  if(!globals.deccelFreq) {
    throw "empty deccelFreq";
  }
  
  if(i !== 0 && i < globals.deccelFreq.length - 1) {
    dec = (globals.deccel[i] + globals.deccel[i-1])/2;
  } else if(i === globals.deccelFreq.length) {
    dec = globals.deccel[i-1];
  } else {
    dec = globals.deccel[i];
  }
  
  spd = speed - dec;
  
  return globals.deccelrateTo(spd, to, length + spd);
};

module.exports = globals;