var getPing = function(tick) {
    return {"msgType": "ping", "gameTick": tick};
  },
  getThrottle = function(value, tick) {
    return {"msgType": "throttle", "data": value, "gameTick": tick};
  },

  getSwitchLane = function(left, tick) {
    return {"msgType": "switchLane", "data": left && "Left" || "Right", "gameTick": tick };
  },

  /**
   * @argument carcount {Number}
   * @argument trackname {string, optional} : "keimola"
   * @argument password {string, optional} : "llamasszirozo"
   */
  getCreateRace = function(carcount, trackname, password) {
    trackname = trackname || "keimola";
    password = password || undefined;
    return {"msgType": "createRace", "data": {
      "botId": {
        "name": process.argv[4],
        "key": process.argv[5]
      },
      "trackName": trackname,
      "password": password,
      "carCount": carcount
    }};
  },

  /**
   * @argument carcount {Number}
   * @argument trackname {string, optional} : "keimola"
   * @argument password {string, optional} : "llamasszirozo"
   */
  getJoinRace = function(carcount, trackname, password) {
    trackname = trackname || "keimola";
    password = password || undefined;
    return {"msgType": "joinRace", "data": {
      "botId": {
        "name": process.argv[4],
        "key": process.argv[5]
      },
      "trackName": trackname,
      "password": password,
      "carCount": carcount
    }};
  },
  
  getJoin = function() {
    return {
      msgType: "join",
      data: {
        name: process.argv[4],
        key: process.argv[5]
      }
    }
  };

module.exports = {
  "ping": getPing,
  
  "throttle": getThrottle,
  
  "switchLane": getSwitchLane,
  
  "createRace": getCreateRace,
  
  "joinRace": getJoinRace,
  
  "join" : getJoin
}