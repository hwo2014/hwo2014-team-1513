var globals = require("./globals"),
  mkdirp = require("./lib/mkdirp"),
  path = require("path"),
  fs = require("fs"),
  carPositions = require("./carpositions");

module.exports =  {
    "join": function(data) {
      console.log("join");
      return false;
    },
  
    "yourCar": function(data) {
      console.log("yourCar");
  
      globals.color = data.color;
      
      return false;
    },
    
    "gameInit" : function(data) {
      
      console.log("gameInit");
      
      globals.loadDec = 1;
      
      globals.race = data.race;
      
      /** kanyarok pontos helye, jó lehet még valamire */
      globals.turnPositions = [];
      
      
      var i,
        j,
        k = null;
      
      for(i = 0; i < data.race.track.pieces.length; i += 1) {
        if(data.race.track.pieces[i].angle) {
          j = i;
        } else {
          k = k === null ? i : k;
          continue;
        }
        
        while(data.race.track.pieces[i].angle && i < data.race.track.pieces.length) {
          i += 1;
        }
        
        i = i - 1;
        
        
        globals.turnPositions.push({
          "from": typeof k === "undefined" ? 0 : k,
          "start": j,
          "end": i
        });
        
        k = null; 
      }
      
      if(k < data.race.track.pieces.length) {
        globals.turnPositions.push({
          "from": k,
          "start": globals.turnPositions[0].start,
          "end": globals.turnPositions[0].end
        });
      }
      
      globals.turnPositions.indexOf = function(turnIndex, carIndex) {
        var i = 0;
        
        for(; i < globals.turnPositions.length; i += 1) {
          if(globals.turnPositions[i].start <= turnIndex &&
            globals.turnPositions[i].end >= turnIndex && 
            globals.turnPositions[i].from === carIndex) {
              return i;
            }
        }
        
        return -1;
      };
      
      //console.log(globals.turnPositions);
      
      //console.log(JSON.stringify(data.race, 0, 2));
      
      mkdirp.sync(path.join(__dirname, "..", "ai", globals.race.track.id));
      
      try {
        globals.deccelFreq = JSON.parse(fs.readFileSync(path.join(__dirname, "..", "ai", globals.race.track.id, "freq.txt")));
      } catch(e){
        globals.loadDec = 0;
      }
      
      try {
        globals.deccel = JSON.parse(fs.readFileSync(path.join(__dirname, "..", "ai", globals.race.track.id, "dec.txt")));
      } catch(e){
        globals.loadDec = 0;
      }
      
      try {
        globals.settings = JSON.parse(fs.readFileSync(path.join(__dirname, "..", "ai", globals.race.track.id, "settings.txt")));
      } catch(e) {}
      
      
      globals.settings = globals.settings || {};
      
      globals.settings.arriveSpeed = globals.settings.arriveSpeed || 2.0;
      
      console.log("setting arrive speed to: " + globals.settings.arriveSpeed);
      
      globals.settings.decimal = globals.settings.decimal || 1;
    },
    
    "gameStart": function(data) {
      console.log("gameStart");
      return false;
    },
    
    "carPositions": carPositions,
    
    "crash": function(data) {
      console.log("crash");
      if(data.color === globals.color) {
        
        globals.settings.arriveSpeed = globals.settings.lastArriveSpeed;
        
        globals.settings.decimal += 1;
        
        console.log("last turn was unsuccess, revert arrive speed to: " + globals.settings.arriveSpeed);
        
        globals.preventRaise = 1;
      }
    },
    
    "spawn": function(data) {
      console.log("spawn");
      return false;
    },
    
    "lapFinished": function(data) {
      
      globals.preventRaise = 0; 
      
      if(!globals.testingDec && globals.settings.decimal < 3 && globals.me.piecePosition.lap !== globals.race.raceSession.laps) {
        globals.settings.lastArriveSpeed = globals.settings.arriveSpeed;
        globals.settings.arriveSpeed += 1 / Math.pow(10,globals.settings.decimal-1);
        console.log("raise arrive speed to: "+globals.settings.arriveSpeed);
      }
      
      console.log("lapFinished", "S:m".replace('S', Math.floor(data.lapTime.millis / 1000)).replace('m', data.lapTime.millis % 1000));
      
      return false;
    },
    
    "gameEnd": function(data) {
      //var i = 0, keys = Object.keys(globals.turns);
      
      fs.writeFileSync(path.join(__dirname, "..", "ai", globals.race.track.id, "freq.txt"), JSON.stringify(globals.deccelFreq));
      fs.writeFileSync(path.join(__dirname, "..", "ai", globals.race.track.id, "dec.txt"), JSON.stringify(globals.deccel));
      
      fs.writeFileSync(path.join(__dirname, "..", "ai", globals.race.track.id, "settings.txt"), JSON.stringify(globals.settings));
    },
    
    "finish": function(data) {
      console.log("finish");
      return false;
    },
    
    "tournamentEnd": function(data) {
      console.log("tournamentEnd");
      return false;
    },
    
    "dnf": function(data) {
      console.log("dnf");
      return false;
    }
};


/**API DOCUMENT

RECEIVE

#join

{"msgType": "join", "data": {
  "name": "Schumacher",
  "key": "UEWJBVNHDS"
}}


#yourCar

{"msgType": "yourCar", "data": {
  "name": "Schumacher",
  "color": "red"
}}


#gameInit

{"msgType": "gameInit", "data": {
  "race": {
    "track": {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ],
      "startingPoint": {
        "position": {
          "x": -340.0,
          "y": -96.0
        },
        "angle": 90.0
      }
    },
    "cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ],
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
  }
}}


#gameStart

{"msgType": "gameStart", "data": null}

#carPositions

{"msgType": "carPositions", "data": [
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
], "gameId": "OIUHGERJWEOI", "gameTick": 0}

#spawn

{"msgType": "spawn", "data": {
  "name": "Rosberg",
  "color": "blue"
}, "gameId": "OIUHGERJWEOI", "gameTick": 150}

#lapFinished

{"msgType": "lapFinished", "data": {
  "car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 666,
    "millis": 6660
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }
}, "gameId": "OIUHGERJWEOI", "gameTick": 300}

#gameEnd

{"msgType": "gameEnd", "data": {
  "results": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "laps": 3,
        "ticks": 9999,
        "millis": 45245
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ],
  "bestLaps": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "lap": 2,
        "ticks": 3333,
        "millis": 20000
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ]
}}


#finish

{"msgType": "finish", "data": {
  "name": "Schumacher",
  "color": "red"
}, "gameId": "OIUHGERJWEOI", "gameTick": 2345}


#tournamentEnd

{"msgType": "tournamentEnd", "data": null}

#dnf

{"msgType": "dnf", "data": {
  "car": {
    "name": "Rosberg",
    "color": "blue"
  },
  "reason": "disconnected"
}, "gameId": "OIUHGERJWEOI", "gameTick": 650}


SEND

$ping
    
{"msgType": "ping"}


$throttle 

{"msgType": "throttle", "data": 0.0 -> 1.0, "gameTick": 666}

    
$switchLane

{"msgType": "switchLane", "data": "Left" || "Right", "gameTick": 666 }
    
    

TEST COMMANDS
    
&createRace

{"msgType": "createRace", "data": {
  "botId": {
    "name": "schumacher",
    "key": "UEWJBVNHDS"
  },
  "trackName": "hockenheimring",
  "password": "schumi4ever",
  "carCount": 3
}}


&joinRace

{"msgType": "joinRace", "data": {
  "botId": {
    "name": "keke",
    "key": "IVMNERKWEW"
  },
  "trackName": "hockenheimring",
  "password": "schumi4ever",
  "carCount": 3
}}
    
*/
