var packet = require("./packet"),
   globals = require("./globals"),

  getPreviousPiece = function(index) {
    var num = index-1;
    return globals.race.track.pieces[num >= 0 ? num : globals.race.track.pieces.length-1];
  },
  getPiece = globals.getPiece,
  isNextNPieceStraight = globals.isNextNPieceStraight,
  
  getNextPiece = function(index) {
    var num = index+1;
    return globals.race.track.pieces[num < globals.race.track.pieces.length ? num : 0];
  },
  
  getNextTurnPiece = function(index) {
    var num = index,
      piece;
      
    while(++num != index) {
      if(num >= globals.race.track.pieces.length) num = 0;
      piece = getPiece(num);
      if(piece.angle) return piece;
    }
    
    return 0;
  },
  
  getAngleBetween2Switches = function(index) {
    var num = index,
      piece,
      sum = 0;
      
    while(++num != index) {
      if(num >= globals.race.track.pieces.length) num = 0;
      piece = getPiece(num);
      
      if(piece.angle) sum += piece.angle;
      
      if(piece.switch) {
        return sum;
      }
    }
    
    return 0;
  },
  
  getLaneDistance = function(lane) {
    var lanes = globals.race.track.lanes;
    
    for(var i = 0; i < lanes.length; i++) {
      if(lanes[i].index == lane) {
        return lanes[i].distanceFromCenter;
      }
    }
  },
  
  getNext3PiecesAngle = function(index) {
    var num = index,
      piece,
      sum = 0;
    
    for(var i = 0; i < 3; i++, ++num) {
      if(num >= globals.race.track.pieces.length) num = 0;
      piece = getPiece(num);
      
      if(piece.angle) sum += piece.angle;
    }
    
    return sum;
  },
  
  getMyCarData = function(data) {
    
    for(var i = 0; i<data.length ; i++) {
      if(data[i].id.color === globals.color) {
        return data[i];
      }  
    }
    
    throw "NOT_FOUND";
  },
  
  ptolemaios = function(a, b, c, d) {
      return Math.sqrt(a * c + b * d);
  },
  getLengthOfTurn = function(radius, angle, startLane, endLane) {
    if(startLane != endLane) {
      var startLLength = getLaneDistance(startLane),
          endLLength = getLaneDistance(endLane),
          laneDiff = Math.abs(startLLength-endLLength),
          startLength = (radius + startLLength) * (Math.PI / 180.0) * Math.abs(angle),
          endLength = (radius + endLLength) * (Math.PI / 180.0) * Math.abs(angle);
          
          
      return ptolemaios(startLength, laneDiff, endLength, laneDiff);
    }
    return (radius + getLaneDistance(startLane)) * (Math.PI / 180.0) * Math.abs(angle);
  },
  getLengthOfStreight = function(length, startLane, endLane) {
    if(startLane != endLane) {
      var startLLength = getLaneDistance(startLane),
          endLLength = getLaneDistance(endLane),
          laneDiff = Math.abs(startLLength-endLLength);
          
      return ptolemaios(length, laneDiff, length, laneDiff);
    }
    return length;
  },
  
  getSpeed = function(data, gameTick) {
    globals.speed = globals.speed || {lastPos : 0, lastPiece : 0, lastStartLane : 0, lastEndLane : 0};
    
    var speed = globals.speed,
      piece = data.piecePosition.pieceIndex,
      lastPiece,
      diff,
      length;
      
    if(speed.lastPiece != piece) {
      lastPiece = getPiece(speed.lastPiece);
      length = getLengthOfStreight(lastPiece.length, speed.lastStartLane, speed.lastEndLane) 
            || getLengthOfTurn(lastPiece.radius, lastPiece.angle, speed.lastStartLane, speed.lastEndLane);
      
      diff = data.piecePosition.inPieceDistance + length - speed.lastPos;
      
    } else {
      diff = data.piecePosition.inPieceDistance - speed.lastPos;
    }
    
    speed.lastPos = data.piecePosition.inPieceDistance;
    speed.lastPiece = piece;
    speed.lastStartLane = data.piecePosition.lane.startLaneIndex;
    speed.lastEndLane = data.piecePosition.lane.endLaneIndex;
    
    return diff;
  },
  
  straightPiecesLength = function(n, index) {
    var len = 0,
      i = 0,
      ind;
      
    for(;i < n; i += 1) {
      ind = (index + i) % globals.race.track.pieces.length;
      if(getPiece(ind).angle) {
        break;
      }
      len += getPiece(ind).length;
    }
    return len;
  };

var carPositions = function(data, gameTick, gameId, send) {
    var me = getMyCarData(data),
      index = me.piecePosition.pieceIndex,
      piece = getPiece(index),
      nextPiece = getNextPiece(index),
      speed = getSpeed(me, gameTick),
      turnNum,
      accel;
    
    globals.me = me;
    globals.deccelFreq = globals.deccelFreq || [];
    globals.deccel = globals.deccel || [];
    globals.settings = globals.settings || {};
    
    globals.preventRaise = typeof globals.preventRaise === "undefined" ? 1 : globals.preventRaise;
    
    globals.testingDec = me.piecePosition.lap < 2 && !globals.loadDec;
    
    globals.lastSpeed = globals.lastSpeed || 0;
    
    accel = speed - globals.lastSpeed;
    
    if((getPreviousPiece(index).angle && !getPiece(index).angle) ||
      (index === 0 && !me.piecePosition.lap)) {
      globals.carIndex = index;
    }
    
    //console.log("v: "+speed+" s: "+me.piecePosition.inPieceDistance + " t: "+gameTick+ " accel: "+ accel);
    
    globals.lastSpeed = speed;
    
    if(accel < 0) {
      globals.insertDeccel(speed, accel);
    }
    
    if(globals.currentIndex != index) {
      console.log('@' + index, '\t',Math.floor(index / globals.race.track.pieces.length * 100) + '%');
      globals.currentIndex = index;
    }
    
    
    if(nextPiece.switch && globals.pieceTurn != index) {
      
      globals.pieceTurn = index;
      
      turnNum = getAngleBetween2Switches(index+1);
      
      if(turnNum) {
        if(turnNum > 0) {
          send(packet.switchLane(0, gameTick));
        } else {
          send(packet.switchLane(1, gameTick));
        }
      }
    }
    
    globals.nextTurn = 0;
      
    while(isNextNPieceStraight(globals.nextTurn, index)) {
      globals.nextTurn += 1;
    }
    
    /** megadja a kanyarodáshoz milyen sebességre kell lelassulni */
    var getTurnArriveSpeed = function(nextTurn, act, speed) {
        
        var turning = globals.turnPositions.indexOf((nextTurn + act - 1) % globals.race.track.pieces.length, globals.carIndex);
        
        if(turning !== -1) {
          
          
          if(globals.turning !== turning) {
          
            globals.turning = turning;
            
            
            globals.settings.arriveSpeed = globals.settings.arriveSpeed || 3.0;
            
          }
          
        }
        
        return globals.settings.arriveSpeed;
      };
    
    globals.restrict = 0;
    
    /**a kanyar felétől gyursulunk, mert mért ne*/
    if(piece.angle && getLengthOfTurn(piece.radius, piece.angle, me.piecePosition.lane.startLaneIndex, me.piecePosition.lane.endLaneIndex) / 2 > me.piecePosition.inPieceDistance) {
      globals.restrict = 0;
    }
      
    if(!globals.testingDec && straightPiecesLength(globals.nextTurn, index) < (globals.deccelrateTo(speed, getTurnArriveSpeed(globals.nextTurn, index, speed), 0))) {
      globals.restrict = 1;
    }
    
    if(globals.testingDec && !globals.testPhaseStart && globals.nextTurn > 1 ) {
      globals.testPhaseStart = 1;
      globals.testPhaseBrake = straightPiecesLength(globals.nextTurn, index) / 1.7;
      globals.testPhaseState = 0;
      console.log("test started, brakeFrom: "+globals.testPhaseBrake);
    }
    
    if(globals.testingDec && globals.nextTurn === 1 && globals.testPhaseStart) {
      globals.testPhaseEnd = 1;
      globals.testPhaseStart = 0;
      console.log("test ended");
    }
    
    if(globals.testingDec && globals.nextTurn === 1 && speed > 3) {
      globals.restrict = 1; 
    }
    
    if(globals.testingDec && globals.testPhaseStart) {
      globals.testPhaseState += speed;
      if(globals.testPhaseBrake < globals.testPhaseState) {
        globals.restrict = 1;
      }
    }
    
    if(globals.restrict) {
      send(packet.throttle(0.0, gameTick));
    } else {
      send(packet.throttle(1.0, gameTick));
    }
    
};


module.exports = carPositions;

/*

#crash

{"msgType": "crash", "data": {
  "name": "Rosberg",
  "color": "blue"
}, "gameId": "OIUHGERJWEOI", "gameTick": 3}


#carPositions

{"msgType": "carPositions", "data": [
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
], "gameId": "OIUHGERJWEOI", "gameTick": 0}

*/