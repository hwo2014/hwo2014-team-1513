var net = require("net"),
  JSONStream = require('JSONStream'),
  serverHost = process.argv[2],
  serverPort = process.argv[3],
  botName = process.argv[4],
  botKey = process.argv[5],
  
  handler = require(__dirname + "/src/handler.js"),
  packet = require(__dirname + "/src/packet"),
  
  client = net.connect(serverPort, serverHost, function() {
    
    if(process.env.CAR_MAP) {
        return send(packet.joinRace(1, process.env.CAR_MAP));
    }
    
    return send(packet.join());
  }),
    
  send = function(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
  },

  jsonStream = client.pipe(JSONStream.parse());

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

jsonStream.on('data', function(obj) {
  
  handler[obj.msgType] && handler[obj.msgType](obj.data, obj.gameTick, obj.gameId, send);
  
  send(packet.ping(obj.gameTick));
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});